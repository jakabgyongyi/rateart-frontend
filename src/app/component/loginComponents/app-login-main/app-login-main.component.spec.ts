import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLoginMainComponent } from './app-login-main.component';

describe('AppLoginMainComponent', () => {
  let component: AppLoginMainComponent;
  let fixture: ComponentFixture<AppLoginMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLoginMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLoginMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
