import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from '../routing/app-routing.module';
import { AppComponent } from '../app.component';
import { AppLoginMainComponent } from '../component/loginComponents/app-login-main/app-login-main.component';

@NgModule({
  declarations: [
    AppComponent,
    AppLoginMainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
